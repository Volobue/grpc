package services

import (
	"context"
	"cv-service/internal/core"
	"cv-service/proto"
)

type ResumeRepository interface {
	GetAll(ctx context.Context) (*[]core.Resume, error)
}

type ResumeService struct {
	proto.ResumeServiceServer
	resumeRepository ResumeRepository
}

func NewResumeService(resumeRepository ResumeRepository) *ResumeService {
	return &ResumeService{
		resumeRepository: resumeRepository,
	}
}

func (service *ResumeService) GetResumes(ctx context.Context, request *proto.ResumeRequest) (response *proto.ResumeResponse, err error) {
	resumes, err := service.resumeRepository.GetAll(ctx)

	if err != nil {
		return &proto.ResumeResponse{}, err
	}

	if resumes == nil {
		resumes = &[]core.Resume{}
	}

	var protoResumes []*proto.Resume
	for _, r := range *resumes {
		protoResumes = append(protoResumes, &proto.Resume{
			Id:                  r.ID.Hex(),
			FirstName:           r.FirstName,
			LastName:            r.LastName,
			Age:                 int32(r.Age),
			Experience:          int32(r.Experience),
			MainLanguage:        r.MainLanguage,
			SecondaryLanguage:   r.SecondaryLanguage,
			ProgrammingLanguage: r.ProgrammingLanguage,
			SalaryFrom:          int32(r.SalaryFrom),
			Location:            r.Location,
			WillingToMoveOut:    r.WillingToMoveOut,
			Gender:              r.Gender,
		})
	}

	return &proto.ResumeResponse{Resumes: protoResumes}, nil
}
