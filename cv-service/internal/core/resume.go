package core

import "go.mongodb.org/mongo-driver/bson/primitive"

type Resume struct {
	ID                  primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	FirstName           string             `bson:"firstName,omitempty" json:"firstName"`
	LastName            string             `bson:"lastName,omitempty" json:"lastName"`
	Age                 int32              `bson:"age,omitempty" json:"age"`
	Experience          int32              `bson:"experience,omitempty" json:"experience"`
	MainLanguage        string             `bson:"mainLanguage,omitempty" json:"mainLanguage"`
	SecondaryLanguage   string             `bson:"secondaryLanguage,omitempty" json:"secondaryLanguage"`
	ProgrammingLanguage string             `bson:"programmingLanguage,omitempty" json:"programmingLanguage"`
	SalaryFrom          int32              `bson:"salaryFrom,omitempty" json:"salaryFrom"`
	Location            string             `bson:"location,omitempty" json:"location"`
	WillingToMoveOut    bool               `bson:"willingToMoveOut,omitempty" json:"willingToMoveOut"`
	Gender              string             `bson:"gender,omitempty" json:"gender"`
}
