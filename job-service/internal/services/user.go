package services

import (
	"context"
	"job-service/internal/core"
	"job-service/proto"
)

type JobRepository interface {
	GetAll(ctx context.Context) (*[]core.Job, error)
}

type JobService struct {
	proto.JobServiceServer
	jobRepository JobRepository
}

func NewJobService(jobRepository JobRepository) *JobService {
	return &JobService{
		jobRepository: jobRepository,
	}
}

func (service *JobService) GetJobs(ctx context.Context, request *proto.JobRequest) (response *proto.JobResponse, err error) {
	jobs, err := service.jobRepository.GetAll(ctx)

	if err != nil {
		jobs = &[]core.Job{}
	}

	var protoJobs []*proto.Job

	for _, r := range *jobs {
		protoJobs = append(protoJobs, &proto.Job{
			Id:                  r.ID.Hex(),
			Name:                r.Name,
			Stack:               r.Stack,
			AgeFrom:             int32(r.AgeFrom),
			Experience:          int32(r.Experience),
			Language:            r.Language,
			ProgrammingLanguage: r.ProgrammingLanguage,
			SalaryFrom:          int32(r.SalaryFrom),
			Location:            r.Location,
			Relocation:          r.Relocation,
			GenderRequired:      r.GenderRequired,
			Insurance:           r.Insurance,
		})
	}

	return &proto.JobResponse{Jobs: protoJobs}, nil
}
