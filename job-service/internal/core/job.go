package core

import "go.mongodb.org/mongo-driver/bson/primitive"

type Job struct {
	ID                  primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Name                string             `bson:"name,omitempty" json:"name"`
	Stack               string             `bson:"stack,omitempty" json:"stack"`
	AgeFrom             int32              `bson:"ageFrom,omitempty" json:"ageFrom"`
	Experience          int32              `bson:"experience,omitempty" json:"experience"`
	Language            string             `bson:"language,omitempty" json:"language"`
	ProgrammingLanguage string             `bson:"programmingLanguage,omitempty" json:"programmingLanguage"`
	SalaryFrom          int32              `bson:"salaryFrom,omitempty" json:"salaryFrom"`
	Location            string             `bson:"location,omitempty" json:"location"`
	Relocation          bool               `bson:"relocation,omitempty" json:"relocation"`
	GenderRequired      string             `bson:"genderRequired,omitempty" json:"genderRequired"`
	Insurance           bool               `bson:"insurance,omitempty" json:"insurance"`
}
