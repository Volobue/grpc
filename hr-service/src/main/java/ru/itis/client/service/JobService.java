package ru.itis.client.service;

import ru.itis.client.grpc.pb.job.JobResponse;

public interface JobService {
    JobResponse getJobs();
}
