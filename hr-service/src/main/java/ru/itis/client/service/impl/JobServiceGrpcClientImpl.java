package ru.itis.client.service.impl;

import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;
import ru.itis.client.grpc.pb.job.JobRequest;
import ru.itis.client.grpc.pb.job.JobResponse;
import ru.itis.client.grpc.pb.job.JobServiceGrpc;
import ru.itis.client.service.JobService;

@Service
public class JobServiceGrpcClientImpl implements JobService {

    @GrpcClient("job-service")
    private JobServiceGrpc.JobServiceBlockingStub jobService;

    @Override
    public JobResponse getJobs() {
        return jobService.getJobs(JobRequest.newBuilder()
                .build());
    }
}
