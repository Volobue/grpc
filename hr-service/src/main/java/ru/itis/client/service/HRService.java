package ru.itis.client.service;

import ru.itis.client.grpc.pb.job.Job;
import ru.itis.client.grpc.pb.resume.Resume;

import java.util.List;
import java.util.Map;

public interface HRService {
    Map<Job, List<Resume>> getCandidates();
}
